import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPersonasInterface } from '../interfaces/personas.interfaces';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  personasInfo: InfoPersonasInterface[] = [];



  constructor(private http:HttpClient) {
    this.cargarPersonas();
   }

   private cargarPersonas(){
    this.http.get('https://martinpage-bdbe6.firebaseio.com/personas.json')
    .subscribe( (resp: InfoPersonasInterface[]) => {
        this.personasInfo = resp;
        console.log(this.personasInfo)
    });

   }
}
