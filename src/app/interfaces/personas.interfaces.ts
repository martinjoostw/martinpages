export interface InfoPersonasInterface {
  frase?: string;
  linkedin?: string;
  nombre?: string;
  profesion?: string;
  url?: string;
}